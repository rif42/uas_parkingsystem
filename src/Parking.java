import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.time.LocalTime;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Objects;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;


public class Parking {
    private JPanel Zky;
    private JTextField tfDate;
    private JLabel lDate;
    private JLabel lBanner;
    private JLabel lNumb;
    private JLabel lType;
    private JTextField tfNumb;
    private JTextField tfFee;
    private JButton btEnterTime;
    private JButton btEdit;
    private JButton btDelete;
    private JButton btPrint;

    private JComboBox<String> cbType;
    private JLabel lRp;
    private JButton btExitTime;
    private JLabel lFee;
    private JTextField tfTime;
    private JTable table1;
    private JPanel buttons;
    private JScrollPane tablepane;
    private JButton btReset;

    String id;

    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://127.0.0.1/parkingsystem";
    static final String USER = "root";
    static final String PASS = " ";
    static Connection conn;
    static Statement stmt;
    static ResultSet rs;
    static PreparedStatement pst;

    public Parking() throws SQLException //constructor
    {
        System.out.println("application starting...");
        connect(); //connecting to database
        btEnterTime.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //declarations
                String policenumber,vehtype,datepattern,mysqlDateString;
                Time jammasuk;
                Date now;
                SimpleDateFormat formatter;

                //assignment
                if (tfNumb.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Please insert vehicle number");
                } else {
                    now = new Date();
                    datepattern = "yyyy-MM-dd";
                    formatter = new SimpleDateFormat(datepattern);
                    mysqlDateString = formatter.format(now);
                    policenumber = tfNumb.getText();
                    vehtype = Objects.requireNonNull(cbType.getSelectedItem()).toString();
                    jammasuk = Time.valueOf(java.time.LocalTime.now());
                    try{
                        //check duplicate
                        stmt = conn.createStatement();
                        PreparedStatement duplicatecheck = conn.prepareStatement("SELECT * FROM parkir WHERE nopol = ?");
                        duplicatecheck.setString(1,policenumber);
                        ResultSet duplicaters = duplicatecheck.executeQuery();
                        if (duplicaters.next()){
                            JOptionPane.showMessageDialog(null, "Data DUPLIKAT");
                        } else { //if no duplicate, write into database
                            try {
                                String sql = "INSERT INTO parkir (nopol,tipe,tanggal,jam_masuk) VALUES (?,?,?,?)";
                                pst = conn.prepareStatement(sql);
                                pst.setString(1, policenumber);
                                pst.setString(2, vehtype);
                                pst.setString(3, mysqlDateString);
                                pst.setTime(4, jammasuk);
                                pst.executeUpdate();
                                JOptionPane.showMessageDialog(null, "Data berhasil ditambahkan");
                                table_load();
                            } catch (SQLException ex) {
                                JOptionPane.showMessageDialog(null, ex);
                            }
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    //gui
                    tfTime.setText(jammasuk.toString());
                    tfDate.setText(mysqlDateString);
                }
            }
        });

        btExitTime.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String policenumber, vehtype;
                Time jamkeluar,jammasuk;
                int timeresult, feemult = 2000;
                if (tfNumb.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Please insert vehicle number");
                } else {
                    policenumber = tfNumb.getText();
                    jamkeluar = Time.valueOf(LocalTime.now());
                    try {
                        stmt = conn.createStatement();
                        PreparedStatement existcheck = conn.prepareStatement("SELECT jam_masuk,tipe FROM parkir WHERE nopol = ?");
                        existcheck.setString(1,policenumber);
                        ResultSet existrs = existcheck.executeQuery();
                        if (existrs.next()){
                            jammasuk = existrs.getTime("jam_masuk");
                            vehtype = existrs.getString("tipe");
                            System.out.println(jammasuk);
                            try {
                                String sql = "UPDATE parkir SET jam_keluar = ? WHERE nopol = ?";
                                pst = conn.prepareStatement(sql);
                                pst.setTime(1, jamkeluar);
                                pst.setString(2, policenumber);
                                pst.executeUpdate();
                                table_load();
                                clear();
                            } catch (SQLException ex) {
                                JOptionPane.showMessageDialog(null, ex);
                            }

                            timeresult = (int) Math.ceil((jamkeluar.getTime() - jammasuk.getTime())/3600000);
                            if (timeresult == 0){
                                timeresult = 1;
                            }

                            if (Objects.equals(vehtype, "motor")){
                                feemult = 2000;
                            } else if (Objects.equals(vehtype, "mobil")){
                                feemult = 4000;
                            } else if (Objects.equals(vehtype, "truck")){
                                feemult = 5000;
                            }

                            int totalfee = timeresult * feemult;

                            try{
                                PreparedStatement insertbiaya = conn.prepareStatement("UPDATE parkir SET biaya_parkir = ? WHERE nopol = ?");
                                insertbiaya.setInt(1,totalfee);
                                insertbiaya.setString(2,policenumber);
                                insertbiaya.executeUpdate();
                                JOptionPane.showMessageDialog(null, "Data berhasil diubah");
                                table_load();
                                clear();
                            }catch (SQLException ex){
                                JOptionPane.showMessageDialog(null, ex);
                            }

                            //gui
                            tfTime.setText(String.valueOf(timeresult));
                            tfFee.setText(String.valueOf(totalfee));
                        } else {
                            JOptionPane.showMessageDialog(null, "Data tidak ditemukan");
                            tfTime.setText("");
                        }
                    } catch (SQLException ex) {
                        throw new RuntimeException(ex);
                    }
                }
            }
        });
        table1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                int row = table1.getSelectedRow();
                id = table1.getValueAt(row, 0).toString();
                String nopol = table1.getValueAt(row, 1).toString();
                String tanggal = table1.getValueAt(row, 2).toString();
                String jammasuk = table1.getValueAt(row, 3).toString();
                String tipe = table1.getValueAt(row, 6).toString();
                tfNumb.setText(nopol);
                cbType.setSelectedItem(tipe);
                tfDate.setText(tanggal);
                tfTime.setText(jammasuk);
                btEdit.setEnabled(table1.getValueAt(row, 4) == null);
            }
        });
        btReset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clear();
            }
        });
        btEdit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nopol = tfNumb.getText();
                String tipe = (String) cbType.getSelectedItem();
                try{
                    PreparedStatement edit = conn.prepareStatement("UPDATE parkir SET nopol = ?, tipe = ? WHERE id = ?");
                    edit.setString(1, nopol);
                    edit.setString(2, tipe);
                    edit.setString(3, id);
                    edit.executeUpdate();
                    JOptionPane.showMessageDialog(null, "Data berhasil diubah");
                    table_load();
                    clear();
                } catch (SQLException ex){
                    JOptionPane.showMessageDialog(null, ex);
                }
            }
        });
        btDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    PreparedStatement edit = conn.prepareStatement("DELETE FROM parkir WHERE id = ?");
                    edit.setString(1, id);
                    edit.executeUpdate();
                    JOptionPane.showMessageDialog(null, "Data berhasil dihapus!");
                    table_load();
                    clear();
                } catch (SQLException ex){
                    JOptionPane.showMessageDialog(null, ex);
                }
            }
        });
        btPrint.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFrame invoice = new JFrame("Invoice");
                invoice.setContentPane(new Invoice(id).invoiceform);
                invoice.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                invoice.pack();
                invoice.setVisible(true);
            }
        });
    }




    public static void main (String[] args) throws SQLException {
        JFrame frame = new JFrame("Parking System");
        frame.setContentPane(new Parking().Zky);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
    public void connect()
    {
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/parkingsystem", "root", " ");
            System.out.println("CONNECTION SUCCESS");
        }
        catch(ClassNotFoundException | SQLException exception){
            System.out.println(exception);
        }
        table_load();
    }

    public void table_load(){
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("ID");
        model.addColumn("Nopol");
        model.addColumn("Tanggal");
        model.addColumn("Jam Masuk");
        model.addColumn("Jam Keluar");
        model.addColumn("Biaya");
        model.addColumn("Jenis Kendaraan");
        try{
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/parkingsystem", "root", " ");
            assert conn != null;
            Statement stmt = conn.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT * FROM parkir ORDER BY id DESC");
            while(rs.next())
            {
                model.addRow(new Object[] {
                        rs.getString("id"),
                        rs.getString("nopol"),
                        rs.getString("tanggal"),
                        rs.getString("jam_masuk"),
                        rs.getString("jam_keluar"),
                        rs.getString("biaya_parkir"),
                        rs.getString("tipe"),
                });
            }

            stmt.close();
            conn.close();
            table1.setModel(model);
        }
        catch(SQLException exception){
            System.out.println(exception);
        }
    }

    public void clear() {
        tfNumb.setText("");
        tfDate.setText("");
        tfTime.setText("");
        tfFee.setText("");
    }
}
