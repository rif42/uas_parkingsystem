import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.time.Instant;
import java.time.LocalTime;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Objects;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import javax.sql.DataSource;

public class Invoice {

    private JLabel lbTitle;
    private JLabel lbSubTitle;
    private JLabel lbVehtype;
    public JPanel invoiceform;
    private JLabel biaya;
    private JLabel jammasuk;
    private JLabel jamkeluar;

    static String id;
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://127.0.0.1/parkingsystem";
    static final String USER = "root";
    static final String PASS = " ";
    static Connection conn;
    static Statement stmt;
    static ResultSet rs;
    static PreparedStatement pst;

    public Invoice(String id){
        System.out.println(id);
        connect();
        try{
            stmt = conn.createStatement();
            PreparedStatement invoice = conn.prepareStatement("SELECT * FROM parkir WHERE id = ?");
            invoice.setString(1,id);
            ResultSet invoicers = invoice.executeQuery();
            while(invoicers.next()){
                lbVehtype.setText(invoicers.getString("tipe"));
                biaya.setText("Rp. "+invoicers.getString("biaya_parkir"));
                jammasuk.setText("Jam Masuk :"+invoicers.getString("jam_masuk"));
                jamkeluar.setText("Jam Keluar: "+invoicers.getString("jam_keluar"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("Invoice");
        frame.setContentPane(new Invoice(id).invoiceform);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public void connect()
    {
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/parkingsystem", "root", " ");
            System.out.println("CONNECTION SUCCESS");
        }
        catch(ClassNotFoundException | SQLException exception){
            System.out.println(exception);
        }
    }


}

